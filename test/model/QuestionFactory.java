/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import org.hychen.exampaper.model.Answer;
import org.hychen.exampaper.model.Question;

/**
 * Generate a Question object
 * @author steven
 */
public class QuestionFactory {
    /**
     * Return a Question object with three options.
     * @return Question object 
     */
    public static Question getQuestion() {
        Answer ans1 = new Answer();
        Answer ans2 = new Answer();
        Answer ans3 = new Answer();
        Question ques = new Question();
        ans1.setContent("This is the first answer");
        ans1.setCorrect(false);
        ans2.setContent("This is the second answer");;
        ans2.setCorrect(true);
        ans3.setContent("This is the third answer");;
        ans3.setCorrect(false);

        ques.add(ans1);
        ques.add(ans2);
        ques.add(ans3);
        ques.setType(Question.MULTIPLE_CHOICES_SINGLE_ANSWER);
        
        return ques;
    }
    
}

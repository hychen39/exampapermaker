package model;


import org.hychen.exampaper.model.Question;
import org.hychen.exampaper.model.Answer;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class QuestionTest {
    private Answer ans1 = new Answer();
    private Answer ans2 = new Answer();
    private Answer ans3 = new Answer();
    private Question ques;
    public QuestionTest() {
    }

    /**
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        ans1.setContent("This is the first answer");    
        ans1.setCorrect(false);
        ans2.setContent("This is the second answer");;
        ans2.setCorrect(true);
        ans3.setContent("This is the third answer");;
        ans3.setCorrect(false);
        ques = new Question();
        ques.add(ans1);
        ques.add(ans2);
        ques.add(ans3);
    }

    /**
     * @see Question#add(Answer)
     */
    @Test
    public void testAdd() {
        //fail("Unimplemented");
        int numAns = ques.getNumOptions();
        assertEquals(numAns, 3);
    }

    /**
     * @see Question#getNumOptionsCorrect()
     */
    @Test
    public void testGetNumCorrAns() {
        //fail("Unimplemented");
        int numCorAns = ques.getNumOptionsCorrect();
        assertEquals(numCorAns, 1);
    }

    /**
     * @see Question#getNumOptions()
     */
    @Test
    public void testGetNumAns() {
        //fail("Unimplemented");
        int numAns = ques.getNumOptions();
        assertEquals(numAns, 3);
    }


    /**Test the function of mixing up option orders.
     *
     * The test will include the following methods {@link model.Question#randomPositions()}.
     *
     * @see Question#getCorrOptRandPos()
     */
    @Test
    public void testGetCorrOptRandPos(){
        // mix up the option positions
        ques.randomPositions();
        String corRespStr ="";
        String originalRespStr ="";
        for(int i=0 ; i < 10 ; i++){
             corRespStr = ques.getCorrOptRandPos();
             originalRespStr = ques.getCorrOptPos();
            if (!corRespStr.equals(originalRespStr)){
                String result = "Assert the original and mixed up response strings. Rand Pos String: " + corRespStr + ". Original correct Option String "
                        + originalRespStr;
                System.out.println(result);
                assertSame(result , true, true);
                return;
            }
            // if the same, mix up again.
            ques.randomPositions();
        }
        String result = "Assert the original and mixed up response strings. Rand Pos String: " + corRespStr + ". Original correct Option String "
                + originalRespStr;
        fail(result);
    }
}

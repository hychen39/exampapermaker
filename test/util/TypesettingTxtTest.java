package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.hychen.exampaper.model.Paper;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TypesettingTxtTest {
    Paper _paper;
    String _filename = "d:\\EBSItemBank1115-Test.xls";
    String _outputFile = "d:\\OutputPaper.txt";
    String _outputFile1 = "d:\\OutputPaper-Rand.txt";
    
    public TypesettingTxtTest() {
        _paper = null;
    }

    @Before
    public void setUp() throws Exception {
        // read a Paper object from a excel file.
        LoaderExcel loader = new LoaderExcel();
        _paper = loader.loadFromExcel(_filename);
    }

    /**
     * @see TypesettingTxt#typeset(String)
     */
    @Test
    public void testTypeset() {
        //fail("Unimplemented");
        TypesettingTxt typeset = new TypesettingTxt();
        typeset.setPaper(_paper);
        try {
            typeset.typeset(_outputFile);
            typeset.typeset(_outputFile1, true);
        } catch (Exception e) {
            fail("Fail to Write the text file!!");
        }
        File outFile = new File(this._outputFile);
        File outFile1 = new File(this._outputFile1);
        boolean existFlag = outFile.exists();
        assertEquals("Assert the exist of the file.", true, existFlag);
        boolean existFlag1 = outFile.exists();
        assertEquals("Assert the exist of the file.", true, existFlag1);
    }
}

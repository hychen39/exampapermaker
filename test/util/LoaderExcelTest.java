package util;

import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.Collection;

import java.util.Iterator;

import org.hychen.exampaper.model.Paper;
import org.hychen.exampaper.model.Question;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
/**
 * Test class @link{utl#LoaderExcel}.
 * Test file puts in d:\EBSItemBank1115-Test.xls
 * 
 * @version $Id: LoaderExcelTest.java 1960 2013-11-25 06:45:40Z steven $
 */
public class LoaderExcelTest {
    private Paper _paper;
    private LoaderExcel _loaderExcel;
    
    public LoaderExcelTest() throws IOException {
        _paper = null;
        _loaderExcel = new LoaderExcel();
    }

    @Before
    public void setUp() throws Exception {
    }

    /**
     * @see LoaderExcel#loadFromExcel(String)
     */
    @Test
    public void testLoadFromExcel() {
        //fail("Unimplemented");
        try {
            // test the number of questions
            _paper = this._loaderExcel.loadFromExcel("d:\\EBS-TestBank-UnitTest-40403.xls");
            assertEquals("Test number of question should be 5.", _paper.getNumofQues(), 5);
            // test 
            Collection<Question> allQues = _paper.getAllQues();
            // test the first question
            Iterator<Question> quesIter = allQues.iterator();
            Question ques = quesIter.next();
            assertEquals("Test the first question. Should have 6 answers", ques.getNumOptions(), 6);
            assertEquals("Should have 4 correct answers", ques.getNumOptionsCorrect(), 4 ); 
            
            
        } catch (FileNotFoundException e) {
            fail("File does not exist");
            
        } catch (IOException e) {
            fail("IO Exceptions");
        }
    }
}

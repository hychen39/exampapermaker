package util;

import java.io.File;

import org.hychen.exampaper.model.Paper;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TypesettingLMSTest {
    Paper _paper;
    //String _filename = "d:\\EBSItemBank1115-Test.xls";
    String _filename = "d:\\SCM-Midterm-LMS-OnlineTest.xls"; 
    String _outputFile = "d:\\OutputPaper-LMS.txt";
    Typesetting typeSetLms  = new TypesettingLMS();

    
    public TypesettingLMSTest() {
        _paper = null;
    }

    @Before
    public void setUp() throws Exception {
        // read a Paper object from a excel file.
        LoaderExcel loader = new LoaderExcel();
        _paper = loader.loadFromExcel(_filename);
    }

  
    /**
     * @see TypesettingLMS#typeset(String)
     */
    @Test
    public void testTypeset() {
        fail("Unimplemented");
    }

    /**
     * @see TypesettingLMS#typeset(String,boolean)
     */
    @Test
    public void testTypeset1() throws Exception {
        //fail("Unimplemented");
        typeSetLms.setPaper(_paper);
        typeSetLms.typeset(_outputFile, true);
        File outFile = new File(this._outputFile);
        boolean existFlag = outFile.exists();
        assertEquals("Assert the exist of the file.", true, existFlag);
    }
}

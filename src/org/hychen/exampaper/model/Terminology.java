/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.hychen.exampaper.model;

/**
 * Terminology data model
 * @author user
 */
public class Terminology {
    private int _id;
    private String _chinese;
    private String _english;
    private String _definition;
    private String _category;

    
    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }
/**
 * 中文術語
 * @return 
 */
    public String getChinese() {
        return _chinese;
    }

    public void setChinese(String _chinese) {
        this._chinese = _chinese;
    }
/**
 * Terminology in English
 * @return 
 */
    public String getEnglish() {
        return _english;
    }

    public void setEnglish(String _english) {
        this._english = _english;
    }

    /**
     * Definition for the terminology
     * @return 
     */
    public String getDefinition() {
        return _definition;
    }

    public void setDefinition(String _definition) {
        this._definition = _definition;
    }

    public String getCategory() {
        return _category;
    }

    public void setCategory(String _category) {
        this._category = _category;
    }
    
    
}

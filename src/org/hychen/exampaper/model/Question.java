package org.hychen.exampaper.model;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * A Question class that contain at least one <code>Answer</code>.
 */
public class Question {
    /**
     * Question type: Multiple Choices with single answer
     */
    public static String MULTIPLE_CHOICES_SINGLE_ANSWER = "MC-S";
    /**
     * Question type: Multiple choices with multiple answers
     */
    public static String MULTIPLE_CHOICES_MULTIPLE_ANSWERS = "MC-M";
    /**
     * Question type: true/false
     */
    public static String TRUE_FALSE = "TF";
    private Collection<Answer> options;
    // number of correct answers in the question
    private int numAns;
    // content of the question
    private String content;
    // numbering of the question
    private int _numbering;
    // response order: Need to randomize?
    private boolean _respOrder;
    // Correct answer position
    private String _corrAnsPos;
    // Concept to test
    private String _concepts;
    // Question type
    private String _type;
    
    // Randomized options
    private Collection<Answer> _randomOptions;
    // String for the correct responses
    private String _corrRandRespOder;
    
    private String _Apps;

    /**
     * Get Set application the question beloing to.
     * @return application string
     */
    public String getAppsCategory() {
        return _Apps;
    }

    /**
     * Set application the question beloing to.
     * @param _Apps 
     */
    public void setAppsCategory(String _Apps) {
        this._Apps = _Apps;
    }

    public void setConcepts(String _concepts) {
        this._concepts = _concepts;
    }

    public String getConcepts() {
        return _concepts;
    }

    public void setRespOrderRandom(boolean _respOrder) {
        this._respOrder = _respOrder;
    }

    public boolean isRespOrderRandom() {
        return _respOrder;
    }

    public Question() {
        super();
        options = new ArrayList<Answer>();
        _randomOptions = null;
        numAns = 0;
        content = "Empty!";
        _numbering = 0;
        _respOrder = false;
        _corrAnsPos = "Empty!";
        _type = Question.MULTIPLE_CHOICES_MULTIPLE_ANSWERS;
        
        
    }

    public void setType(String _type) {
        this._type = _type;
    }

    public String getType() {
        return _type;
    }

    /**
     * Add an answer.
     *
     */
    public void add(Answer ans){
        options.add(ans);
    }
    /**
     * Get the number of correct answers.
     * 
     * @return Number of correct answers
     */
    public int getNumOptionsCorrect(){
        if (this.numAns != 0)
            return this.numAns;
        
        int count = 0;
        for (Answer ans: options){
           if(ans.isCorrect()) 
               count++;
        };
        this.numAns = count;
        return this.numAns;
    }
    /**
     * Get the number of available answers.
     * 
     * @return Number of available answers.
     */
    public int getNumOptions(){
        return options.size();
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setOptions(Collection<Answer> options) {
        this.options = options;
    }

    public Collection<Answer> getOptions() {
        return options;
    }

    public void setNumbering(int _numbering) {
        this._numbering = _numbering;
    }

    public int getNumbering() {
        return _numbering;
    }
    /** Set the string of the correct answers. 
     * Multiple answers are splited by commas. For example: 1, 2, 3.
     * @param _corrAnsPos
     */
    public void setCorrOptPos(String _corrAnsPos) {
        this._corrAnsPos = _corrAnsPos;
    }

    /**
     * get positions for the correct options
     * @return position string for correct options separted by semicolons. 
     */
    public String getCorrOptPos() {
        if (!_corrAnsPos.equals("Empty!"))
            return _corrAnsPos;
        _corrAnsPos = this.makeCorrOptString(this.options);
        return _corrAnsPos;
    }

   /** Get the options with random positions.
     *  Run {@link #randomOptions} to mix up again.
     *
     * @return Options with random positions.
     */
    @SuppressWarnings("oracle.jdeveloper.java.semantic-warning")
    public Collection<Answer> getOptionsRP() {
       if (_randomOptions == null)
           this.randomPositions();
        return _randomOptions;
    }
   
   /**
    * Mix up the order of the responses.
    */
   public void randomPositions(){
       //Answer[] oriOptions = this.options.toArray(new Answer[this.options.size()]);
       //ArrayList<Answer> origOptArrayList = new ArrayList<Answer>(Arrays.asList(oriOptions));
       //Answer[] randOptions = this._randomOptions.toArray(new Answer[this._randomOptions.size()]);
       ArrayList<Answer> randOptArrayList = new ArrayList<Answer>();
       randOptArrayList.addAll(this.options);
      
       Collections.shuffle(randOptArrayList);
       this._randomOptions  = randOptArrayList;
       
       this._corrRandRespOder ="";  
   }
   /**
     * Get the string of the positions of the correct options after mixing up the order of the positions.
     * Run @link{#randomOptions} method before using the method. 
     * 
     * @return String of the positions of the correct options that have been randomized.
     */
    public String getCorrOptRandPos() {
        // get the string of the correct options
       if (!this._corrRandRespOder.isEmpty())
            return this._corrRandRespOder;
       // make the string
        String corrStr="";
        int pos = 1;
        for (Answer ans: _randomOptions){
            if (ans.isCorrect()){
               corrStr += pos ;
               corrStr += ";";
            }
            pos++;
        }
        this._corrRandRespOder = corrStr.substring(0, corrStr.length() -1 );
        return _corrRandRespOder;
    }
   
    /**
     *  Make the string of the correct option positions. use semi-colons as the delimiters.
     *  
     *  @param src A set of options.
     */

    private String makeCorrOptString(Collection<Answer> src){
            String corrStr="";
            int pos = 1;
            for (Answer ans: src){
                if (ans.isCorrect()){
                   corrStr += pos ;
                   corrStr += ";";
                }
                pos++;
            }
            return  corrStr.substring(0, corrStr.length() -1 );

    }
}





package org.hychen.exampaper.model;

import java.util.ArrayList;
import java.util.Collection;

/**
 * A exam paper.
 * The paper contains: header and questions.
 * 
 * @version $Id: Paper.java 1958 2013-11-25 06:43:47Z steven $
 */
public class Paper {
    private String _header;
    private Collection<Question> _questions;
    private Collection<Terminology> _matching;

    /**
     * Match questions with correct answers. 配對題
     * @return 
     */
    public Collection<Terminology> getMatching() {
        return _matching;
    }

    public void setMatching(Collection<Terminology> _matching) {
        this._matching = _matching;
    }
    
    private boolean _isMixup;
    
    public Paper() {
        super();
        // init member filds;
        _header = "Header";
        _questions = new ArrayList();
        _isMixup = false;
    }
    
    /**
     * Addd a question to the paper.
     */
    public void add(Question ques) {
        this._questions.add(ques);
    }
    /**
     * Get the number of questions in the paper.
     */
    public int getNumofQues(){
      return _questions.size();  
    }
    /**
     * Get all question in the paper.
     * 
     */
    public Collection<Question> getAllQues(){
        return _questions;
    }

    /** Set to Ture to make the paper mixes up the orders of options for each question.
     *  The default is false.
     *  
     * @param isMixup
     */
    public void setIsMixup(boolean isMixup) {
        this._isMixup = isMixup;
    }

    public boolean isIsMixup() {
        return _isMixup;
    }
}

package org.hychen.exampaper.model;

/**
 * An answer for a question.
 */
public class Answer {
    // The content for the answer
     String content;
    // True means the answer is correct.
    boolean correct;
    public Answer() {
        super();
        this.setCorrect(false);
        this.setContent("Empty");
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public boolean isCorrect() {
        return correct;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.hychen.exampaper.dbUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hychen.exampaper.model.Answer;
import org.hychen.exampaper.model.Question;

/**
 * Question Table. <br>
 * Table to save Question object. There are 18 columns in the table.
 * @author steven
 */
public class QuestionTable {
    
    static public final String TABLE_NAME = "QUESTION";
    static public final String SEQUENCE_NAME = "QUESTION_SEQ";
    static public final int TOTAL_COLUMNS = 18;
    
    private Connection _conn;
    private PreparedStatement _insertQuestion;
    /**
     * Add a record to the table.
     * 
     * @param srcQues Question object to be saved
     * @return 1 if inserted, 0 otherwise 
     */
    public int addQuestion(Question srcQues){
        String sqlStatement = "insert into " + QuestionTable.TABLE_NAME + " values(";
        sqlStatement += "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        int recordCount = 0;  // record count
        try {
            _insertQuestion  = _conn.prepareStatement(sqlStatement);
        } catch (SQLException ex) {
            Logger.getLogger(QuestionTable.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            _insertQuestion.setInt(1, srcQues.getNumbering()); // Question ID
            _insertQuestion.setString(2, srcQues.getContent());  //question description
            _insertQuestion.setString(3, srcQues.getType());  // question type
            // concept
            _insertQuestion.setString(4, srcQues.getConcepts());
            //
            _insertQuestion.setInt(5, srcQues.getNumOptions());
            //
            _insertQuestion.setInt(6, srcQues.getNumOptionsCorrect());
            // column 7
            _insertQuestion.setString(7, srcQues.getCorrOptPos());
            // column 8
            String isRandom = srcQues.isRespOrderRandom()?"T":"F";
            _insertQuestion.setString(8, isRandom);
// Column 8
            // available options in the question 
            Collection<Answer> options = srcQues.getOptions();
            int currentColumnPos = 9;
            for (Answer opt: options){
                _insertQuestion.setString(currentColumnPos, opt.getContent());
                currentColumnPos++;
            }
            // reserved columns for options
            for (int i=currentColumnPos ; i <= QuestionTable.TOTAL_COLUMNS ; i++){
                _insertQuestion.setString(i, null);
            }
            recordCount = _insertQuestion.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(QuestionTable.class.getName()).log(Level.SEVERE, null, ex);
        }
        return recordCount;
    }
/**
     * Return the next available question id.
     *
     * @return Integer as Question id
     * @throws java.sql.SQLException
     */
    public int suggestQuestionID() throws SQLException {
        int questionID = 0;
        String qry = "Select max(QID) from Question";
        Statement stmt = _conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet rs = stmt.executeQuery(qry);
        rs.first();
        questionID = rs.getInt(1)+1;
        return questionID;

    }

    /**
     * Constructor.
     * Use {@link DBConnMgr} to get Connection object.
     * 
     * @param conn Connection to the targeted database
     */
    public QuestionTable(Connection conn) {
        _conn = conn;
    }

    
    
}

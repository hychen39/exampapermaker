/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author user
 */
public class ExtensionFileFilter extends FileFilter {
private String description;
private String extensions[];
    
    @Override
    public boolean accept(File f) {
        if(f.isDirectory()) {
            return true;
        } else {
            // the target is a file
            String path = f.getAbsolutePath().toLowerCase();
            // check
            for (int i=0; i< this.extensions.length; i++){
                String extName = this.extensions[i];
                int dotPos = path.length() - extName.length() -1;
                if (path.endsWith(extName) && 
                        (path.charAt(dotPos)== '.')) {
                    return true;
                } 
            }
        }
        
        return false;
        
    }

    @Override
    public String getDescription() {
        return description;
    }

    public ExtensionFileFilter(String description, String[] extensions) {
        if (description == null){
          this.description = extensions[0] + "{" + extensions.length + "}";
        } else {
            this.description = description;
        }
        // clone array
        this.extensions = (String []) extensions.clone();
        for(int i=0 ; i < this.extensions.length ; i++){
            this.extensions[i] = extensions[i].toLowerCase();
        }
    }
    
    
}

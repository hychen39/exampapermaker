/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.hychen.exampaper.model.Terminology;

/**
 * 配對題出題. 包括: 題目, 選項, 及答案三個部份。
 * @author user
 */
public class MatchTypeSetTxt extends Typesetting {
    

    @Override
    void typeset(String filename) throws Exception {
        Collection<Terminology> terms = super.getPaper().getMatching();
        
        // generate a sequence of random number
        ArrayList<Integer> optionIndex = new ArrayList<Integer>();
        for (int i = 0; i < terms.size() ; i++){
            optionIndex.add(new Integer(i));
        }
        Collections.shuffle(optionIndex);
        //
        // print out the questions
        
        File txtFilename = new File(filename);
        PrintWriter outFile = new PrintWriter(txtFilename);
        outFile.printf("Match the following questions with correct answers. %n%n");
        int idCount = 1;
        for(Terminology term: terms){
            outFile.printf("%d._____ %s", idCount, term.getChinese());
            if ((idCount % 4)!=0) {
                outFile.printf("\t");
            } else {
                outFile.println();
            }
            idCount++;
        }
        outFile.print("\r\n\r\n");
        
        // print the available options
        int ansCount =1;
        Terminology [] options = terms.toArray(new Terminology[0]);
        Map<Integer, Integer> solutions = new HashMap<Integer, Integer>();
        
        for(Integer idx: optionIndex){
            outFile.printf("%d.%s\t", ansCount, options[idx.intValue()].getEnglish());
            solutions.put(idx, new Integer(ansCount));
            ansCount++;
        }
        outFile.print("\r\n\r\n");
        
        // print the solutions
        for (int i = 0; i < terms.size() ; i++){
            outFile.print(solutions.get(new Integer(i)).intValue());
            outFile.print("; ");
        }
        
        // close file
        outFile.flush();
        outFile.close();
    }

    @Override
    void typeset(String filename, boolean isMixup) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

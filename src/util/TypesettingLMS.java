package util;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

import java.util.Collection;

import org.hychen.exampaper.model.Answer;
import org.hychen.exampaper.model.Paper;
import org.hychen.exampaper.model.Question;

/**
 * 用來產生要匯入 LMS 系統的測驗檔案
 *
 * ilms 出題格式:　<br>
 * 題目匯入步驟及格式說明: <br>
 * 建立一個題目CSV檔案, 如 question.csv, 其格式如下。格式說明: <br>
 * 總共需要 14 個欄位, 各欄位以逗號相隔, 分別是: <br>
 * 題型,題目,選項 1,選項 2,選項 3,選項 4,選項 5,選項 6,選項 7,選項 8,選項 9,選項 10,答案,解說 <br>
 * 其中, 題型: 1(單選), 2(複選), 3(問答), 4(是非), 5(填充); 最後一個選項(解說)可以省略 <br>
 * 範例:<br>
 * 1,3+5=?,6,7,8,9,10,,,,,,3 <br>
 * 2,哪些是昆蟲?,螞蟻,蜘蛛,蝴蝶,蛇,,,,,,,1;3,蛇是爬蟲類 <br>
 * 資料本身中不可有逗號,如果該欄無資料,請留空白(如範例所示); 複選題答案, 請以分號( ; )相隔 <br>
 *
 */

public class TypesettingLMS  extends Typesetting{
    
    public TypesettingLMS() {
        super();
        
    }
    
    public void setPaper(Paper _paper) {
        super.setPaper(_paper);
        
    }

    @Override
    void typeset(String filename) throws Exception {
        // TODO Implement this method
        System.out.println("Not implement yet!!");
        throw new Exception("Not implement yet!!");
        
    }

    @Override
    void typeset(String filename, boolean isMixup) throws Exception {
        // TODO Implement this method
        // check the Paper object
        if (this.getPaper() == null)
            throw new Exception("No Paper object are provided!!");
        // no mix the option positions
        if (! isMixup){
            this.typeset(filename);
            return;
        }
        //Open file
        File outfile = new File(filename);
        PrintWriter outStream = new PrintWriter(new FileWriter(outfile));
        //get questions from paper
        Collection<Question> allQues = this.getPaper().getAllQues();
        int numbering = 0;
        for(Question ques: allQues){     
            // mix up the options positions
            ques.randomPositions();
            StringBuffer strBuf = new StringBuffer();
            // 決定題型 1: 單選, 2: 複選
            int quesType = 1;
            if (ques.getType().toUpperCase().contentEquals("MC-M"))
                quesType = 2;  // Multiple answers

            strBuf.append(doubleQuote(quesType+""));
            strBuf.append(",");
            strBuf.append(doubleQuote(ques.getContent().replaceAll("\n", "")));  // the content of the questions 
            strBuf.append(",");
            // make the answers. 
            // 10 answers are needed for the format.
            Collection<Answer> options = ques.getOptionsRP();
            int optSeq = 1;
            for (Answer option: options){
                strBuf.append(doubleQuote(option.getContent().replaceAll("\n", "")));
                strBuf.append(",");
                optSeq++;
            }
            for (int i=optSeq; i<=10; i++){
                strBuf.append(",");
            }
            // Append answer (13rd column)
            String ans = doubleQuote(ques.getCorrOptRandPos());
            strBuf.append(ans + ",");
            // Append description (the last column)
            strBuf.append(doubleQuote(ques.getConcepts().replaceAll("\n", "")));
            // write to file
            outStream.println(strBuf.toString());
        };
        
        //close file
        outStream.close();
    }
    
    private String doubleQuote(String src){
        return "\"" + src + "\"";
    }
}

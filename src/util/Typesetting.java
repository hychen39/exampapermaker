package util;

import org.hychen.exampaper.model.Paper;

public abstract class Typesetting {
    private Paper _paper;

    public void setPaper(Paper _paper) {
        this._paper = _paper;
    }

    public Paper getPaper() {
        return _paper;
    }

    public Typesetting() {
        super();
        _paper = null;
    }
     abstract void typeset(String filename) throws Exception  ;
     abstract void typeset(String filename, boolean isMixup) throws Exception ;
        

}

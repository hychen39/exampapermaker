/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.hychen.exampaper.model.Terminology;
import org.omg.CORBA.TCKind;

/**
 * Load data from the first sheet in a specified excel file.
 *
 * @author user
 * @see Terminology
 */
public class TermLoaderExcel {

    final public static int ID_POS = 0;
    final public static int CHINESE_POS = 1;
    final public static int ENGLISH_POS = 2;
    final public static int DEFINITION_POS = 3;
    final public static int CATEGORY_POS = 4;

    private String _filename;
    private List<Terminology> _terms;
    private Logger _logger;

    public TermLoaderExcel(String _filename) {
        this._filename = _filename;
        _terms = new ArrayList<Terminology>();
        try {
            setupLogger();
        } catch (IOException ex) {
            Logger.getLogger(TermLoaderExcel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void setupLogger() throws IOException {
        FileHandler logFileHandler = new FileHandler("LoaderExcel.log", true);
        _logger = Logger.getLogger("LoaderExcel");
        _logger.addHandler(logFileHandler);
        logFileHandler.setFormatter(new SimpleFormatter());
    }

    public List<Terminology> load() throws FileNotFoundException, IOException {
        //open file
        //Create a file stream
        FileInputStream file = new FileInputStream(new File(_filename));

        // get the workbook instance for XLS file
        HSSFWorkbook workbook = new HSSFWorkbook(file);

        // get the first sheet.
        HSSFSheet sheet = workbook.getSheetAt(0);
        // get the iterator for rows
        Iterator<Row> rowIterator = sheet.iterator();

        // skip the header
        rowIterator.next();
        // Start to read questions
        _logger.log(Level.INFO, "Start to read data from file: {0}", _filename);
        int rowCount = 0;
        while (rowIterator.hasNext()) {
            Terminology aTerm = new Terminology();
            Row dataRow = rowIterator.next();
            //
            Cell cell1 = dataRow.getCell(TermLoaderExcel.ID_POS);
            double value = cell1.getNumericCellValue();
            aTerm.setId(new Double(value).intValue());
            //
            Cell cell2 = dataRow.getCell(TermLoaderExcel.CHINESE_POS);
            if (cell2 != null) {
                aTerm.setChinese(cell2.getStringCellValue());
            }
            //
            Cell cell3 = dataRow.getCell(TermLoaderExcel.ENGLISH_POS);
            if (cell3 != null) {
                aTerm.setEnglish(cell3.getStringCellValue());
            }
            //
            Cell cell4 = dataRow.getCell(TermLoaderExcel.DEFINITION_POS);
            if (cell4 != null) {
                aTerm.setDefinition(cell4.getStringCellValue());
            }
            //
            Cell cell5 = dataRow.getCell(TermLoaderExcel.CATEGORY_POS);
            if (cell5 != null) {
                aTerm.setCategory(cell5.getStringCellValue());
            }
            //
            _terms.add(aTerm);
            rowCount++;
            _logger.log(Level.INFO, "Read row {0} with ID {1}.", new Object[]{rowCount, aTerm.getId()});
        }
        file.close();
        return _terms;
    }

}

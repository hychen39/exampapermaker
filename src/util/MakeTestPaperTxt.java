package util;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.hychen.exampaper.model.Paper;
import util.TypesettingTxt;


/**
 * Generate a test paper into a text file.
 * @version $Id: MakeTestPaperTxt.java 1964 2013-12-03 14:01:51Z steven $
 */
public class MakeTestPaperTxt {
    private static Logger _logger = null;
    public MakeTestPaperTxt() {
        super();
    }

    public static Logger getLogger() {
        return _logger;
    }

    public static void setLogger(Logger _logger) {
        MakeTestPaperTxt._logger = _logger;
    }

    /** Generate a test paper in the pure text format to a file.
     * Require to give two parameters. The first is the question bank file in the .xls format. The second 
     * is the output file name.
     * 
     * Features for the generated test paper:
     * <ul>
     *  <li> Random option order for each question. </li>
     *  <li> Output to a text file. </li>
     * </ul>
     * @param args
     */
    public static void main(String[] args) throws Exception {
//        MakeTestPaperTxt makeTestPaperTxt = new MakeTestPaperTxt();
        // check arguments. Should be two arguments.
        if (args.length != 1)
            throw new Exception("Should have one argument that specifies the target excel file!!");
        
        // load the file to data model
        LoaderExcel loder = new LoaderExcel();
        Paper paper = loder.loadFromExcel(args[0]);
        
        System.out.format("Select %d questions from excel file %s \n", loder.getNumOfSelected(), args[0]);
        if(_logger != null){
            _logger.log(Level.INFO, "Select #{0} questions from excel file #{1}", 
                    new Object[]{loder.getNumOfSelected(), args[0]});
        }
        // typeset to a text file
        String txtFilename = args[0] + "-txt.txt";
       TypesettingTxt typeset = new TypesettingTxt();
       typeset.setPaper(paper);
       typeset.typeset(txtFilename, true);
       System.out.format("Output text file to %s \n", txtFilename);
       if(_logger != null){
            _logger.log(Level.INFO, "Output text file to #{0} \n", 
                    new Object[]{txtFilename});
        }
       
       // output to LMS file format
       String lmsFilename = args[0] + "-lms.txt";
        TypesettingLMS lmsTypeset = new TypesettingLMS();
        lmsTypeset.setPaper(paper);
        lmsTypeset.typeset(lmsFilename, true);
        System.out.format("Output LMS format file to %s \n", lmsFilename);
        if(_logger != null){
            _logger.log(Level.INFO, "Output LMS format file to #{0} \n", 
                    new Object[]{lmsFilename});
        }
    
        
        
    }
}

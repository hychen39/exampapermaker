package util;

import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import java.io.FileWriter;

import java.io.IOException;

import java.io.PrintWriter;

import java.util.Collection;

import org.hychen.exampaper.model.Answer;
import org.hychen.exampaper.model.Paper;
import org.hychen.exampaper.model.Question;

/**
 * 用來產生測驗卷的文字檔.
 * Given a @code{model#Paper} object, the class will output content of the Paper object to a text file. The content is
 * ready to be a paper for testing.
 *
 * @version $Id: TypesettingTxt.java 1965 2013-12-03 14:04:14Z steven $
 */
public class TypesettingTxt {
    private Paper _paper;
    public TypesettingTxt() {
        super();
        _paper = null;
    }
    
    public void typeset(String filename) throws FileNotFoundException, IOException, Exception {
        // check the Paper object
        if (_paper == null)
            throw new Exception("No Paper object are provided!!");
        //Open file
        File outfile = new File(filename);
        PrintWriter outStream = new PrintWriter(new FileWriter(outfile));
        //get questions from paper
        Collection<Question> allQues = _paper.getAllQues();
        int numbering = 0;
        for(Question ques: allQues){      
            StringBuffer strBuf = new StringBuffer();
            numbering++;
            strBuf.append(numbering + ". ");
            strBuf.append(ques.getContent() + ":\n");  // the content of the questions
            // make the answers
            Collection<Answer> options = ques.getOptions();
            int optSeq = 1;
            for (Answer option: options){
                strBuf.append("("+ optSeq + ")" + option.getContent() + " ");
                optSeq++;
            }
            // write to file
            outStream.println(strBuf.toString() + "\n");
        };
 
        //close file
        outStream.close();
    }

    /** Typeset to a file with the option of random options for each question.
     * 會在檔案的最後面附上答案。
     * @param filename Output filename
     * @param isMixup set True to randomize the order of options for each question. 
     * @throws FileNotFoundException
     * @throws IOException
     * @throws Exception
     */
    public void typeset(String filename, boolean isMixup) throws FileNotFoundException, IOException, Exception {
        // check the Paper object
        if (_paper == null)
            throw new Exception("No Paper object are provided!!");
        // no mix the option positions
        if (! isMixup){
            this.typeset(filename);
            return;
        }
        //Open file
        File outfile = new File(filename);
        PrintWriter outStream = new PrintWriter(new FileWriter(outfile));
        //get questions from paper
        Collection<Question> allQues = _paper.getAllQues();
        int numbering = 0;
        StringBuffer ansStrBuf = new StringBuffer();  // use to store answers for the paper.
        int answerCount = 0;
        for(Question ques: allQues){     
            // mix up the options positions
            ques.randomPositions();
            StringBuffer strBuf = new StringBuffer();
            numbering++;
            strBuf.append(numbering + ". ");
            strBuf.append(ques.getContent() + ":\n");  // the content of the questions
            // make the answers
            Collection<Answer> options = ques.getOptionsRP();
            int optSeq = 1;
            for (Answer option: options){
                strBuf.append("("+ optSeq + ")" + option.getContent() + " ");
                if (option.getContent().length() > 10) // 長度大於 10 個字的選項要換行.
                    strBuf.append("\n");
                optSeq++;
            }
            // write to file
            outStream.println(strBuf.toString() + "\n");
            // make ansers
            ansStrBuf.append("("+ numbering + ")" + ques.getCorrOptRandPos() + " ");
            // make a break for every 10 answers
            if (answerCount == 9){
                ansStrBuf.append("\n");
                answerCount = 0;
            } else
                answerCount++;
            
        };
    
    // Append answers to the end of the file.
        outStream.print(ansStrBuf.toString());
        //close file
        outStream.close();
    }



    public void setPaper(Paper _paper) {
        this._paper = _paper;
    }
}

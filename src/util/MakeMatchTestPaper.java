/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hychen.exampaper.model.Paper;
import org.hychen.exampaper.model.Terminology;

/**
 * Generate a paper with matching test
 *
 * @author user
 */
public class MakeMatchTestPaper {

    public static void main(String[] args) throws Exception {
        // load from file
        // check arguments. Should be two arguments.

        if (args.length != 1) {
            throw new Exception("Should have one argument that specifies the target excel file!!");
        }

        // load the file to data model
        String filename = args[0];
        TermLoaderExcel loader = new TermLoaderExcel(filename);
        List<Terminology> testQuestions = loader.load();

        Paper testPaper = new Paper();
        testPaper.setMatching(testQuestions);

        MatchTypeSetTxt typeSetter = new MatchTypeSetTxt();
        typeSetter.setPaper(testPaper);
        String outFilename = args[0] + "-txt.txt";
        typeSetter.typeset(outFilename);

    }

}

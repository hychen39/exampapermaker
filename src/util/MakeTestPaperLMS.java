package util;

import org.hychen.exampaper.model.Paper;

public class MakeTestPaperLMS {
    public MakeTestPaperLMS() {
        super();
    }

    public static void main(String[] args) throws Exception {
        MakeTestPaperLMS makeTestPaperLMS = new MakeTestPaperLMS();
        if (args.length != 2)
            throw new Exception("Should have to arguments");
        
        // load the file to data model
        LoaderExcel loder = new LoaderExcel();
        Paper paper = loder.loadFromExcel(args[0]);
        
        // typeset to a text file
        TypesettingLMS typeset = new TypesettingLMS();
        typeset.setPaper(paper);
        typeset.typeset(args[1], true);
    }
}

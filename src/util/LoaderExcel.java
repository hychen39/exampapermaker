package util;

import java.io.File;
import java.io.FileInputStream;

import java.io.FileNotFoundException;

import java.io.IOException;

import java.util.Iterator;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.hychen.exampaper.model.Answer;
import org.hychen.exampaper.model.Paper;
import org.hychen.exampaper.model.Question;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 * Load questions for paper from a Excel file. The data should be placed in the first sheet.
 * The file contains the following columns: <br>
 * <ol>
 *  <li> #: the numbering of a question. </li>
 *  <li> Question text: the text for a question </li>
 *  <li> Response type: MC-M means with more than one answers. (複選題); MC-S means with sigle answer (單選題) </li>
 *  <li> Pts: the points for the question </li>
 *  <li> Response Order: the order of answers in the question. Can be Randomed or Fixed. </li>
 *  <li> 1st Response Feed Back: preserve for future use. </li>
 *  <li> 2nd Response Feed Back: preserve for future use. </li>
 *  <li> Response number(s): the (original) positions of the correct answer in the question. </li>
 *  <li> Response 1 to 6: Question 1 to Question 6. </li>
 *  <li> Apps: Application category the question belong to. </li>
 *  <li> Concepts: concepts to test in the question. </li>
 *  <li> Include: is to include to test paper. </li>
 * </ol>
 *
 * @author Hung-Yi Chen
 * @version $Id: LoaderExcel.java 1961 2013-11-29 02:43:38Z steven $
 */
public class LoaderExcel {
    final int  INCLUDE_CELL_COLUMN_POS = 21;  //zero column base
    
    private Logger _logger;
    
    private String _filename;
    private Paper _paper;
    
    private int _readedColumnCount;
    private int _readedRowCount;
    private int _numOfSelected;

    
    public int getRowCount() {
        return _readedRowCount;
    }
    
    /**
     * Return the number of questions loaded from the excel file.
     * @return number of questions
     */
    public int getNumOfSelected(){
        return _numOfSelected;
    }
    
        
    public LoaderExcel() throws IOException {
        super();
        _paper = new Paper();
        _filename = "";
        
        FileHandler logFileHandler = new FileHandler("LoaderExcel.log", true);
        _logger = Logger.getLogger("LoaderExcel");
        _logger.addHandler(logFileHandler);
        logFileHandler.setFormatter(new SimpleFormatter());
        
        _readedColumnCount = 0;
        _readedRowCount = 0;
        _numOfSelected = 0;
        
        
    }
    /**
     * Load data from a excel file. <br>
     * Reading process is logged to file LoaderExcel.log.
     * 
     * @param filename
     */
    public Paper loadFromExcel(String filename) throws FileNotFoundException, IOException {
        //Create a file stream
        FileInputStream file = new FileInputStream(new File(filename));
        
        // get the workbook instance for XLS file
        HSSFWorkbook workbook = new HSSFWorkbook(file);
        
        // get the first sheet.
        HSSFSheet sheet = workbook.getSheetAt(0);
        // get the iterator for rows
        Iterator<Row> rowIterator = sheet.iterator();
        
        
        // skip the header
        rowIterator.next();
        // Start to read questions
        _logger.info("Start to read...");
        
        while (rowIterator.hasNext()){
            
            //get cell iterator
            Row curRow = rowIterator.next();
            
            logReadRow();
            
            // check if to include the question
            Cell cellInclude = curRow.getCell(INCLUDE_CELL_COLUMN_POS);
            double isInclude = (cellInclude == null)? 0 : cellInclude.getNumericCellValue();
            if (isInclude < 1){
                _logger.log(Level.INFO, "Row #{0} is skiped", _readedRowCount);
                continue;
            }
            _numOfSelected++;  
            
            // Start to create a question instance;
            Question ques = new Question();
            
            Iterator<Cell> cellIterator = curRow.cellIterator();
            // get the first cell - first column: the numbering
            Cell cell = cellIterator.next();
            
            int numbering = new Double(cell.getNumericCellValue()).intValue();
            if (numbering == 0)
                break;  // exit the while loop 
            ques.setNumbering(numbering);
            logReadColumn(numbering);
            
            // get the second colunm
            cell = cellIterator.next();
            logReadColumn();
            ques.setContent(cell.getStringCellValue().trim());
            // get the third column -- Response Type
            cell = cellIterator.next();
            logReadColumn();
            ques.setType(cell.getStringCellValue().trim());
            // get the fourth column
            cell = cellIterator.next();
            logReadColumn();
            // get the fifth column: Response Order
            cell = cellIterator.next();
            logReadColumn();
            if (cell.getStringCellValue().toUpperCase().equals("RANDOM")) {
                ques.setRespOrderRandom(true);
            } else {
                ques.setRespOrderRandom(false);
            }
            // get the 6th column: 1st Response Feed Back
            cell = cellIterator.next();logReadColumn();
            // get the 7th column: 1st Response Feed Back
            cell = cellIterator.next();
            logReadColumn();
            // get the 8th column: Position of the correct answer(s)
            cell = cellIterator.next();
            logReadColumn();
            String ansPosStr = "";
            try {	
                ansPosStr = cell.getStringCellValue();   
            }
            catch(Exception e)
            {
                if (cell.getCellType() == 0){
                  int val = (int) cell.getNumericCellValue();
                  ansPosStr = String.valueOf(val);
                };
            }
            String [] ansPos = ansPosStr.split(",");
            ques.setCorrOptPos(ansPosStr);
            // get the  answer 1 to answer 10
            for (int i =0; i<10 ; i++){
                cell = cellIterator.next();
                logReadColumn();
                Answer ans = new Answer();
                if (!cell.getStringCellValue().equals("N/A")){
                ans.setContent(cell.getStringCellValue().trim());
                // check if the answer is a correct one
                ans.setCorrect(false);
                for (String pos : ansPos){
                    if (Integer.parseInt(pos.trim()) == (i+1)){
                        ans.setCorrect(true);
                        break;
                    }
                }
                // add the answer to the question
                ques.add(ans);
                }
            };
            // get apps column
            cell = cellIterator.next();
            logReadColumn();
            ques.setAppsCategory(cell.getStringCellValue().trim());
            // get Concept column
            cell = cellIterator.next();
            logReadColumn();
            ques.setConcepts(cell.getStringCellValue().trim());
            // Complete reading a question
            // add to the paper
            this._paper.add(ques);
            resetColumnCount();
        }
        // close the file
        file.close();
        return this._paper;
    }
    
    /**
     * Log the current column # to be read.
     */
    private void logReadColumn(){
        _readedColumnCount++;
        _logger.log(Level.INFO, "Read Column #{0}..", _readedColumnCount);
    }
    
    private void logReadColumn(int questionId){
        _readedColumnCount++;
        _logger.log(Level.INFO, "Read Column #{0}, Question #{1}..", new Object[]{_readedColumnCount, questionId});
    }
    /**
     * Log the current row # to be read.
     */
    private void logReadRow(){
        _readedRowCount++;
        _logger.log(Level.INFO, "Read Row #{0}..", _readedRowCount);
    }
    
    private void resetColumnCount(){
        _readedColumnCount = 0;
    }
    
    private void resetRowCount(){
        _readedRowCount = 0;
    }
}

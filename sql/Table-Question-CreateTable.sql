create table question( qid Integer primary key, 
                       description VARCHAR2(1000),
                       qtype VARCHAR2(20) not null,
                       concept varchar2(100),
                       numOptions Integer not null,
                       numCorectOptions INTEGER default 1 not null,
                       correctOptionPos VARCHAR2(50),
                       isRandomPos varchar2(1) default 'F' not null,
                       opt1 varchar2(200),
                       opt2 varchar2(200),
                       opt3 varchar2(200),
                       opt4 varchar2(200),
                       opt5 varchar2(200),
                       opt6 varchar2(200),
                       opt7 varchar2(200),
                       opt8 varchar2(200),
                       opt9 varchar2(200),
                       opt10 varchar2(200)
                       );
                       
comment on table question is 'Questions';
comment on column QUESTION.QID is 'Question ID';
comment on column Question.qtype is 'Question type';
comment on column Question.concept is 'Concepts for the question. Express the concept by the following format: CP1::CP2::CP3';
comment on column QUESTION.NUMOPTIONS is 'Number of options in the question';
comment on column QUESTION.NUMCORECTOPTIONS is 'Number of CORRECT  options in the question';
comment on column QUESTION.CORRECTOPTIONPOS is 'Positions for the correct options in the question';
COMMENT on column QUESTION.ISRANDOMPOS is 'Enable randomizing positions of correct options';
comment on column QUESTION.OPT1 is 'First option';

